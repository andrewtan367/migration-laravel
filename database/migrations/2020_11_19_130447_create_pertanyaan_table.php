<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePertanyaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pertanyaan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("judul");
            $table->longText("isi");
            $table->timestamps();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('profil_id')->nullable();
            
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('profil_id')->references('id')->on('profil');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pertanyaan');

        Schema::dropIfExists('pertanyaan', function (Blueprint $table) {
            $table->dropForeign(['user_id', "profil_id"]);
            $table->dropColumn(['user_id', "profil_id"]);
        });

    }
}
