<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLikeDislikePertanyaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('like_dislike_pertanyaan', function (Blueprint $table) {
            $table->bigInteger("poin");
            $table->unsignedBigInteger('profil_id');
            $table->unsignedBigInteger('pertanyaan_id');
            $table->primary(['profil_id', 'pertanyaan_id']);

            $table->foreign('profil_id')->references('id')->on('profil');
            $table->foreign('pertanyaan_id')->references('id')->on('pertanyaan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('like_dislike_pertanyaan');

        Schema::dropIfExists('like_dislike_pertanyaan', function(Blueprint $table) {
            $table->dropForeign(['profil_id']);
            $table->dropForeign(['pertanyaan_id']);
            $table->dropColumn(['profil_id']);
            $table->dropColumn(['pertanyaan_id']);
        });
    }
}
