@extends('layouts.master')

@section('content')
<section class="mx-3 pt-3">
    <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Buat Pertanyaan Baru</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/pertanyaan" method="POST">
            @csrf
          <div class="card-body">
            <div class="form-group">
              <label for="title">Judul</label>
              <input type="text" class="form-control" id="judul" name="judul" placeholder="Enter Judul" value="{{old("judul", "")}}">

              @error('judul')
                <div class="alert alert-danger mt-2">{{ $message }}</div>
              @enderror
            </div>
            <div class="form-group">
              <label for="body">Isi</label>
            <input type="text" class="form-control" id="isi" name="isi" placeholder="Enter isi" value="{{old("isi", "")}}">
              
            @error('isi')
                <div class="alert alert-danger mt-2">{{ $message }}</div>
              @enderror
            </div>
          </div>
          <!-- /.card-body -->
    
          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Create</button>
            <a class="btn btn-primary ml-2" href="/pertanyaan" role="button">Go Back</a>
          </div>
        </form>
      </div>
</section>
@endsection