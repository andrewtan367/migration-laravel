@extends('layouts.master')

@section('content')
<section class="mx-3 pt-3">
    <div class="card">
        <div class="card-header">
          <h3 class="card-title">Tabel Pertanyaan</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          @if (session("success"))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{session("success")}}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
          </div>
          @endif
          <a class="btn btn-primary mb-2" href={{route("pertanyaan.create")}} role="button">New Question</a>
          <table class="table table-bordered">
            <thead>                  
              <tr>
                <th style="width: 10px">#</th>
                <th>Judul</th>
                <th>Isi</th>
                <th style="width: 40px">Actions</th>
              </tr>
            </thead>
            <tbody>
              @forelse ($display as $key => $post)
                  <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$post->judul}}</td>
                    <td>{{$post->isi}}</td>
                    <td class="d-flex">
                        <a class="btn btn-info btn-sm mr-2" href={{route("pertanyaan.show", ["pertanyaan"=>$post->id])}} role="button" st>Show</a>
                        <a class="btn btn-warning btn-sm mr-2" href={{route("pertanyaan.edit", ["pertanyaan"=>$post->id])}} role="button">Edit</a>
                        <form action={{route("pertanyaan.destroy", ["pertanyaan"=>$post->id])}} method="POST">
                            @csrf
                            @method("DELETE")
                            <input type="submit" value="Delete" class="btn btn-sm btn-danger">
                        </form>
                    </td>
                  </tr>
                  @empty
                    <tr>
                        <td colspan="4" align="center">No Questions</td>
                    </tr>
              @endforelse
            </tbody>
          </table>
        </div>
      </div>
</section>
@endsection