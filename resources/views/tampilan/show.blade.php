@extends('layouts.master')

@section('content')
    <div class="class mx-3 pt-3">
        {{-- <h4>Author: {{Auth::user()->name}}</h4> --}}
        <h4>Author: {{$show->nama_author->name}}</h4>
        <h4>Judul: {{$show->judul}}</h4>
        <h4>Isi: {{$show->isi}}</h4>
        <h4>Jawaban Benar: {{$show->jawaban_benar->isi}}</h4>
    </div>
@endsection