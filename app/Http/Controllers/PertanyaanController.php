<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Question;
use App\Jawaban;
use Auth;

class PertanyaanController extends Controller
{
    // autentikasi
    public function __construct() {
        $this->middleware('auth')->except(["index"]);
    }

    public function create() {
        return view("tampilan.create");
    }

    public function store(Request $request) {
        $request->validate([
            "judul" => 'bail|required|unique:pertanyaan',
            "isi" => 'required',
        ]);

        // $query = DB::table("pertanyaan")->insert([
        //     "judul" => $request["judul"],
        //     "isi" => $request["isi"]
        // ]);

        $store = new Question;
        $store->judul = $request["judul"];
        $store->isi = $request["isi"];

        // ngisi foreign key user_id
        $store->user_id = Auth::user()->id;
        $store->save();

        return redirect("/pertanyaan")->with("success", "Pertanyaan berhasil disimpan");
    }

    public function index() {
        // $display = DB::table("pertanyaan")->get(); // kalau di sql-> SELECT * FROM nama_table
        
        $user= Auth::user();
        // $display = Question::all();

        $display = $user->list_pertanyaan;
        // dd($display);
        return view("tampilan.index", compact("display"));
    }

    public function show($pertanyaan_id) {
        // $show = DB::table("pertanyaan")->where("id", $pertanyaan_id)->first();
        
        $show = Question::find($pertanyaan_id);

        // buat ngecek isi dari 
        //  
        return view("tampilan.show", compact("show"));
    }

    public function edit($pertanyaan_id) {
        // $show = DB::table("pertanyaan")->where("id", $pertanyaan_id)->first();
        
        $show = Question::find($pertanyaan_id);
        return view("tampilan.edit", compact("show"));
    }

    public function update($pertanyaan_id, Request $request) {
        $request->validate([
            "judul" => 'bail|required',
            "isi" => 'required',
        ]);

        // $update = DB::table("pertanyaan")
        //             ->where("id", $pertanyaan_id)
        //             ->update([
        //                 "judul" => $request["judul"],
        //                 "isi" => $request["isi"]
        //             ]);

        Question::where("id", $pertanyaan_id)->update([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);
        
        return redirect("/pertanyaan")->with("success", "Pertanyaan berhasil di-update");
    }

    public function destroy($pertanyaan_id) {
        // $destroy = DB::table("pertanyaan")->where("id", $pertanyaan_id)->delete();
        
        Question::destroy($pertanyaan_id);
        return redirect("pertanyaan")->with("success", "Pertanyaan berhasil di-delete");
    }
}
