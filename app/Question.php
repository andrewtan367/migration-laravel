<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Jawaban;
use App\User;

class Question extends Model
{
    protected $table = "pertanyaan";

    public function jawaban_benar() {
        return $this->hasOne("App\Jawaban", "pertanyaan_id");
    }

    public function nama_author() {
        return $this->belongsTo("App\User", "user_id");
    }
}
