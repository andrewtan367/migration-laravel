<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource("pertanyaan", "PertanyaanController");

// // Create pertanyaan
// Route::get("/pertanyaan/create", "PertanyaanController@create");

// // Menampilkan data table pertanyaan di web
// Route::get("/pertanyaan", "PertanyaanController@index");

// // menyimpan data di table pertanyaan yg ada di DB
// Route::post("/pertanyaan", "PertanyaanController@store");

// // Menampilkan secara spesifik pertanyaan
// Route::get("/pertanyaan/{pertanyaan_id}", "PertanyaanController@show");

// // Menampilkan form untuk edit pertanyaan dengan id tertentu
// Route::get("/pertanyaan/{pertanyaan_id}/edit", "PertanyaanController@edit");

// // Update pertanyaan
// Route::put("/pertanyaan/{pertanyaan_id}", "PertanyaanController@update");

// // Delete Pertanyaan
// Route::delete("/pertanyaan/{pertanyaan_id}", "PertanyaanController@destroy");


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
